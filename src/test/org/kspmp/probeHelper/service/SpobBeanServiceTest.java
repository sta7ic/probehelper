package org.kspmp.probeHelper.service;

import com.google.appengine.api.datastore.Entity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.kspmp.core.spob.Spob;
import org.kspmp.probeHelper.service.SpobBeanService;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by sta7ic on 3/8/2016.
 */
public class SpobBeanServiceTest {
    private final LocalServiceTestHelper helper =
            new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    @Before
    public void setUp() {
        helper.setUp();
    }

    @After
    public void tearDown() {
        helper.tearDown();
    }
    @Test
    public void testGetSpobData(){
        SpobBeanService svc = new SpobBeanService();
        svc.writeDemoSpobData();

        List<Entity> spobData = null;
        spobData = svc.getSpobData();
        assertNotNull("Null SpobData result", spobData);
        assertFalse("Empty spotData list", spobData.isEmpty());
    }
    @Test
    public void testGetKerbinData(){
        SpobBeanService svc = new SpobBeanService();
        svc.writeDemoSpobData();

        Entity kerbinE = svc.getByName("Kerbin");
        assertNotNull("Could not get 'Kerbin' by name", kerbinE);

        Spob kerbinS = svc.convertEntityToSpob(kerbinE);
        assertNotNull("Could not convert Kerbin to a Spob", kerbinS);

        Entity kerbinID = svc.getById(4);
        assertNotNull("Could not get spobId=4", kerbinID);
    }
}