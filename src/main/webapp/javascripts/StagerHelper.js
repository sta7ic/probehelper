/**
 * Created by Matt Hauer on 11/6/2015.
 */

function doGetStages(){
    var localDoc = theDoc;
    if(theDoc.lastMnvr === undefined){
        console.log("No maneuvers, cannot run");
        return;
    }
    console.log('in doGetStages...');
    var reqObj = new Object();
    reqObj.payload = theDoc.mass;
    reqObj.payloadSize = "SMALL";
    reqObj.maneuvers = theDoc.lastMnvr;
    reqObj.start = theDoc.origin; // TODO this MUST conform to MisstionStateDto for MissionBuilder
    var someData = new Object();
    someData.theData = JSON.stringify(reqObj);
    //var reqStr = JSON.stringify(someData);
    //console.log('req: '+reqStr);
    $.getJSON('/stager', someData, handleGetStagesResponse);
}

function handleGetStagesResponse(data) {
    console.log('in handleGetStagesResponse');
    var theDiv = $('#stagerDiv');
    var theHtml = '<h5>Stages</h5>';
    theHtml += '<table><thead><td>Stage</td><td>Dry (t)</td><td>Wet (t)</td><td>TWR</td></thead>';
    // {"stages":[{"engineCount":0,"actualSymmetry":0,"dryMass":0.185,"wetMass":0.585,"fuelMass":0.0,"twr":0.3486209275138217,"dv":0.0}]}
    var stages = data.stages;
    for (var i = 0; i < stages.length; ++i) {
        // dataHtml += '<tr><td>' + man.dv.toFixed(2) + ' m/s</td><td>' + man.desc + '</td></tr>';
        theHtml += '<tr><td>'+i+'</td><td>'+stages[i].dryMass.toFixed(1)+'</td><td>'+stages[i].wetMass.toFixed(1)+'</td><td>'+stages[i].twr.toFixed(3)+'</td></tr>';
    }
    theHtml += '</table>';
    theDiv.html(theHtml);
    theDiv.show();
}