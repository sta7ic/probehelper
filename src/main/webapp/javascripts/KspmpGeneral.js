/**
 * Created by sta7ic on 12/12/2015.
 */

function writeRadios(theDiv, inHead, inName, inClass, parts){
    theDiv.innerHTML = '<h5>'+inHead+'</h5>';
    for(i = 0; i < parts.length; ++i){
        var add = '<p><input type="radio" name="'+inName+'" class="'+inClass+'" value="';
        add += i;
        add += '">';
        add += parts[i].pLabel;
        add += '</input></p>';
        theDiv.innerHTML += add;
    }
}

function writeCheckers(theDiv, inHead, inName, inClass, parts){
    theDiv.innerHTML = '<h5>'+inHead+'</h5>';
    for(i = 0; i < parts.length; ++i){
        var add = '<p><input type="checkbox" name="'+inName+'" class="'+inClass+'" value="';
        add += i;
        add += '">';
        add += parts[i].pLabel;
        add += '</input></p>';
        theDiv.innerHTML += add;
    }
}