<%--
  Created by IntelliJ IDEA.
  User: Matt Hauer
  Date: 3/8/2016
  Time: 6:47 PM
  The JSP for the "Pathfinder" page, used to specify inputs for a desired interplanetary path in KSP
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pathfinder</title>
    <link href="CSS/probeStyles.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
</head>
<body>
    <div class="probeSummary">
        <p>Pathfinder -- A Tool for SQUAD's Kerbal Space Program</p>
        <p>Developed by Matt Hauer -- <a href="mailto:matthew.hauer@gmail.com">matthew.hauer@gmail.com</a></p>
        <p>Progress Blog: <a href="http://kspmp.blogspot.com/">Blogger</a></p>
        <p>bitbucket project: <a href="https://bitbucket.org/sta7ic/kspmp">kspmp</a></p>
        <p>Probe Helper: <a href="probe.html">over here</a></p>
    </div>
    <div class="pathfinderInputCon">
        <div class="pathfinderInput">
            <h4>Origin</h4>
            <div class="probeInput">
                <p><input type="checkbox" id="departTimeCheck" onchange="departViz();"><label for="departTimeCheck">Specify Departure Time</label></p>
                <div id="departTimeDiv" style="display: none">
                <p><label for="departYearBox">Year: </label><input type="text" id="departYearBox" size="4"></p>
                <p><label for="departDayBox">Day: </label><input type="text" id="departDayBox" size="4"></p>
                <p><label for="departHourBox">Hour: </label><input type="text" id="departHourBox" size="4"></p>
                </div>

                <div class="probeInput">
                    <p><label for="originMode">Origin State:</label><select id="originMode">
                        <option value="0">Landed</option>
                        <option value="1">Orbit</option>
                    </select>
                    </p>
                    <p><label for="originSpob">Origin Body:</label><select id="originSpob">
                    </select>
                    </p>
                    <p><label for="originPeriBox">Periapsis: </label><input type="number" id="originPeriBox" min="0" value="0"></p>
                    <p><label for="originApoBox">Apoapsis: </label><input type="number" id="originApoBox" min="0" value="0"></p>
                </div>
            </div>
        </div>
        <div class="pathfinderInput">
            <h4>Destination</h4>
            <div class="probeInput">
            <p><input type="checkbox" id="arrivalTimeCheck" onchange="arriveViz();">Specify Arrival Time
            <div id="arrivalTimeDiv" name="arrivalTimeDiv" style="display: none">
            <p>Year: <input type="text" id="arrivalYearBox" size="4">
            Day: <input type="text" id="arrivalDayBox" size="4">
            Hour: <input type="text" id="arrivalHourBox" size="4"></p>
            </div>
            <div class="probeInput">
                <p><select id="destinationMode">
                    <option value="0">Flyby</option>
                    <option value="1">Orbit</option>
                    <option value="2">Landed</option>
                    <option value="3">Impacter</option>
                </select>
                     <%--TODO map this to location goals in javascript --%>
                </p>
                <p><select id="destinationSpob">
                </select>
                </p>
                <p>Periapsis: <input type="number" id="destinationPeriBox" min="0" value="0"></p>
                <p>Apoapsis: <input type="number" id="destinationApoBox" min="0" value="0"></p>
            </div>
        </div>
    </div>
    <div id="outputDivCon" class="pathfinderOutputCon">
        <p>Output stuff here</p>
        <div class="pathfinderOutput">
            <p>Pathfinder output div.</p>
        </div>
    </div>
    <div id="controlDiv">
    <button type="button" id="getManeuverBtn">Get Maneuvers</button>
    <p>Response: <span id="responseSpan"></span>
    </p>
    <div id="errorDiv" class="probeSummary" hidden><p>error stub</p></div>
    <div id="maneuverDiv" class="probeSummary" hidden><p>data stub</p></div>
    </div>
    <%--<div>--%>
    <%--<button type="button" id="getStagesBtn">Get Stages</button>--%>
    <%--<div id="stagerDiv"></div>--%>
    <%--<div id="stagerErrorDiv"></div>--%>
    <%--</div>--%>
    </div>
    <%--<script src="javascripts/KspmpGeneral.js"></script>--%>
    <%--<script src="javascripts/ProbeHelper.js"></script>--%>
    <%--<script src="javascripts/StagerHelper.js"></script>--%>
    <script src="javascripts/PathfinderHelper.js"></script>
</body>
</html>
