package org.kspmp.probeHelper.service;

import com.google.appengine.api.datastore.*;
import org.kspmp.core.spob.Spob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by sta7ic on 12/18/2015.
 */
public class SpobBeanService {
    List<Entity> spobData;
    PreparedQuery spobResults;
    Map<String, Entity> spobNameMap;
    Map<Long, Entity> spobIdMap;
    DatastoreService ds;

    public SpobBeanService(){
        ds = DatastoreServiceFactory.getDatastoreService();
        spobNameMap = new HashMap<>();
        spobIdMap = new HashMap<>();
    }

    public List<Entity> getSpobData(){
        if(spobData == null) {
            fetchSpobData();
        }
        if(spobData != null){
            return spobData;
        } else {
            // TODO log error
            return new ArrayList<Entity>();
        }
    }

    public Entity getByName(String name){
        return spobNameMap.get(name);
    }

    // wrap it up to avoid user casting
    public Entity getById(int id){
        return getById(new Long(id));
    }

    public Entity getById(Long id){
        return spobIdMap.get(id);
    }

    public void fetchSpobData(){
        // TODO this causes the result to come back blank
        Query spobQuery = new Query("SpobData").addSort("spobId", Query.SortDirection.ASCENDING);
//        Query spobQuery = new Query("SpobData");
        spobResults = ds.prepare(spobQuery);
//        return spobResults.asList(FetchOptions.Builder.withDefaults());
        spobData = spobResults.asList(FetchOptions.Builder.withDefaults());

        // prepare maps
        for(Entity e : spobData) {
            if (e.hasProperty("spobName")) {
                spobNameMap.put((String) e.getProperty("spobName"), e);
            }
            if (e.hasProperty("spobId")) {
                spobIdMap.put((Long) e.getProperty("spobId"), e);
            }
        }
    }

    public void writeDemoSpobData() {
        Entity kerbol = new Entity("SpobData");
        kerbol.setProperty("spobName", "Kerbol");
        kerbol.setProperty("spobId", 0);
        kerbol.setProperty("parentSpob", -1);
        kerbol.setProperty("radius", 261600000);
        kerbol.setProperty("gm", 1.17E+18);
        ds.put(kerbol);

        Entity kerbin = new Entity ("SpobData");
        kerbin.setProperty("spobName", "Kerbin");
        kerbin.setProperty("spobId", 4);
        kerbin.setProperty("parentSpob", 0);
        kerbin.setProperty("radius", 600000.0);
        kerbin.setProperty("gm", 3.53E+12);
        ds.put(kerbin);

        fetchSpobData();
    }

    public Spob convertEntityToSpob(Entity e){
        String name = "(error!)";
        Double rad = Double.NaN;
        Double gm = Double.NaN;
        if(e.hasProperty("spobName"))
            name = (String) e.getProperty("spobName");
        if(e.hasProperty("radius"))
            rad = (Double) e.getProperty("radius");
        if(e.hasProperty("gm"))
            gm = (Double) e.getProperty("gm");
        Spob s = new Spob(name, rad, gm);

        // TODO convert e into a Bean, DTO, or JSON, then into a Spob.
        
        return s;
    }
}
