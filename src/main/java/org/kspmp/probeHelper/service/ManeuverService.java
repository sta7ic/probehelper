package org.kspmp.probeHelper.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.kspmp.maneuver.IManeuver;
import org.kspmp.maneuver.ManeuverDto;
import org.kspmp.mission.Mission;
import org.kspmp.pathfinder.Pathfinder;
import org.kspmp.pathfinder.PathfinderFactory;

import java.util.List;

/**
 * Created by Matt Hauer on 10/29/2015.
 */
public class ManeuverService {

    // TODO characterize input
    // TODO characterize output
    public static JsonObject findPath(JsonObject reqData) {
        // extract 'origin'
        // extract 'destination'
        // run kspmp.Pathfinder
        Pathfinder pather = PathfinderFactory.build(reqData);
        if(pather == null){
            // log error
            JsonObject err = new JsonObject();
            err.addProperty("error", "Unable to build Pathfinder from request");
            return err;
        }
        if(!pather.isGoodPath()){
            JsonObject err = new JsonObject();
            err.addProperty("error", "BadPath: "+pather.getStatus());
            return err;
        }
        Mission path = pather.getPath();
        List<ManeuverDto> mDtoList = path.getManeuverDtos();
        ManeuverDto[] mDtoArray = mDtoList.toArray(new ManeuverDto[0]);

        JsonElement maneuversElem = new Gson().toJsonTree(mDtoArray);
        JsonArray maneuversArray = null;
        if(maneuversElem.isJsonArray()){
            maneuversArray = maneuversElem.getAsJsonArray();
        } else {
            JsonObject err = new JsonObject();
            err.addProperty("error", "could not convert maneuver Dtos into JsonArray");
            return err;
        }

        // serialize maneuver list

        // return Json array
        JsonObject jo = new JsonObject();
        jo.add("maneuvers", maneuversArray);
        return jo;
    }
}
