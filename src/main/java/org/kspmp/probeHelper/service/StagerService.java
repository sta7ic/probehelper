package org.kspmp.probeHelper.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.kspmp.core.SolarSystem;
import org.kspmp.mission.Mission;
import org.kspmp.mission.MissionBuilder;
import org.kspmp.mission.MissionDto;
import org.kspmp.stager.Stage;
import org.kspmp.stager.StageDto;
import org.kspmp.stager.Stager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matt Hauer on 11/6/2015.
 */
public class StagerService {
    public static JsonObject buildStages(JsonObject reqData) {
        JsonObject rv = new JsonObject();
        System.out.println("in buildStages");
        MissionDto missionDto = new Gson().fromJson(reqData, MissionDto.class);
        System.out.println("missonDto: "+missionDto.getDesc());
        Mission mission = MissionBuilder.buildNoOrigin(missionDto, SolarSystem.getDefaultSystem());
        mission.setMinThrust(0.4);
        // send both to Stager
        Stager stager = new Stager(mission);
        List<Stage> stages = stager.buildStages();
        stager.report();

        // iterate through stages, convert to StageDto or something
        List<StageDto> stageDtos = new ArrayList<StageDto>();
        for(Stage s : stages){
            stageDtos.add(s.asDto());
        }

        // serialize response
        JsonElement e = new Gson().toJsonTree(stageDtos);
        JsonObject jo = new JsonObject();
        jo.add("stages", e);
        return jo;
    }
}
/*
* Notes:
* 2015/11/24: null pointer at MissionBuilder.build():35.  No ScriptStart defined, so it just plain breaks.  need to
*   write the origin into the MissionDto or the Mission object.
* */
