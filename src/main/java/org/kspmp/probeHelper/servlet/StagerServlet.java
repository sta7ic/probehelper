package org.kspmp.probeHelper.servlet;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.kspmp.probeHelper.service.StagerService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Matt Hauer on 11/6/2015.
 */
public class StagerServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("in StagerServlet");

        String data = request.getParameter("theData");

        if(data != null) { // do response
//        System.out.println("data = "+data);
            JsonObject reqData = new JsonParser().parse(data).getAsJsonObject();
            JsonObject pathResp = StagerService.buildStages(reqData);
            // TODO write into response
            if(pathResp.has("error")){
                ;
            }
            response.getWriter().write(new Gson().toJson(pathResp));
        } else {
            System.out.println("could not find 'theData'");
        }
    }
}
