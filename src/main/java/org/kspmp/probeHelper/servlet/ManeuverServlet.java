package org.kspmp.probeHelper.servlet;


import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.kspmp.probeHelper.service.ManeuverService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by sta7ic on 10/25/2015.
 */
public class ManeuverServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = "/maneuver.jsp";

        getServletContext().getRequestDispatcher(url).forward(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("in ManeuverServlet");

        String data = req.getParameter("theData");
        if(data != null) { // do response
//        System.out.println("data = "+data);
            JsonObject reqData = new JsonParser().parse(data).getAsJsonObject();
            JsonObject pathResp = ManeuverService.findPath(reqData);
            // TODO write into response
            if(pathResp.has("error")){
                ;
            }
            resp.getWriter().write(new Gson().toJson(pathResp));
        } // end if data != null
        else {
            JsonObject stub = new JsonObject();
            stub.addProperty("status", "normal");
            resp.getWriter().write(new Gson().toJson(stub));
        }
    }
}
