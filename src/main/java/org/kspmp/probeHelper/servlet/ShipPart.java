package org.kspmp.probeHelper.servlet;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.Key;

import java.lang.String;
import java.util.Date;
import java.util.List;

/**
 * Created by sta7ic on 10/25/2015.
 */
@Entity
public class ShipPart {
    @Id public Long id;
    public Double mass, cost, power, energy, data, xmit;
    public String pName, pLabel;
    public String partType; // should be enum?

    public ShipPart(){
        ;
    }

    public ShipPart(String n, String l, double m, double c, double p, double e, double d, double x){
        pName = n;
        pLabel = l;
        mass = m;
        cost = c;
        power = p;
        energy = e;
        data = d;
        xmit = x;
    }
}


/**
 * The @Entity tells Objectify about our entity.  We also register it in {@link OfyHelper}
 * Our primary key @Id is set automatically by the Google Datastore for us.
 *
 * We add a @Parent to tell the object about its ancestor. We are doing this to support many
 * guestbooks.  Objectify, unlike the AppEngine library requires that you specify the fields you
 * want to index using @Index.  Only indexing the fields you need can lead to substantial gains in
 * performance -- though if not indexing your data from the start will require indexing it later.
 *
 * NOTE - all the properties are PUBLIC so that can keep the code simple.
 **/

